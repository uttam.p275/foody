#imports
from flask import Flask, g,redirect, url_for, session
from flask import url_for
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from flask import Flask, render_template, request
import random
import json
import os



app = Flask(__name__)
app.secret_key = os.urandom(24)

db = SQLAlchemy(app)

#SQL Alchemy Engine
app.config['SQLALCHEMY_DATABASE_URI']='mysql+mysqlconnector://root:root@localhost:3306/uttam'
engine = create_engine('mysql+mysqlconnector://root:root@localhost:3306/uttam')





#Database Model
class food_list(db.Model):
	id_of_food = db.Column(db.Integer, primary_key=True)
	name_restaurant = db.Column(db.String(200), nullable=False)
	name = db.Column(db.String(200))
	picture = db.Column(db.String(200), nullable=False)
	rating = db.Column(db.Integer, nullable=False)

class my_cart(db.Model):
	order_id = db.Column(db.Integer, primary_key=True)
	food_ordered = db.Column(db.String(200), nullable=False)
	price = db.Column(db.Integer, primary_key=True)

#Routes
#route for entry
@app.route('/api/v1/entry',methods=['GET','POST'])
def send():
	if request.method == 'POST':
		nameOfRestaurant = request.form['name_restaurant']
		nameOfFood = request.form['name']
		image_file = request.form['food_name']
		rating_of_food = request.form['rating_name']
		rand_id = random.randint(0,10000)
		user = food_list(id_of_food=rand_id,name_restaurant=nameOfRestaurant,name=nameOfFood,picture=image_file,rating=rating_of_food)
		db.session.add(user)
		db.session.commit()
		return "Successfully added"
	return render_template('index.html')

#route for search
@app.route('/api/v1/track',methods=['GET','POST'])
def receive():
	if request.method == 'POST':
		name_restaurant = request.form['name_of_restaurant']
		name_restaurant = '"'+name_restaurant+'"'
		
		with engine.connect() as con:
			rs = con.execute('SELECT * FROM food_list where name_restaurant='+name_restaurant)
		a=""
		for row in rs:
			a=a+str(row)

		b = a.replace("'", '')
		c = b.replace(",", '')
		d = c.replace("(", '')
		e = d.replace(")", '')
		f = e.split()
		return render_template('index_display.html', part_one=f[0],part_two=f[1],part_three=f[2],part_four=f[3],part_five=f[4])
	return render_template('index2.html')

#route for restaurant search with restaurant details
#Same as track
@app.route('/api/v1/added_to_cart',methods=['GET','POST'])
def send_twos():
	if request.method == 'POST':
		#nameOfFoodOrdered = request.form['food_item']
		nameOfFoodOrdered = 'masala_dosa' #We need to pass this from the existing page #Parked
		temp = random.randint(0,10000000)
		temp_json = json.dumps({"order_id":temp})
		order_generated = my_cart(order_id=temp,food_ordered=nameOfFoodOrdered,price="100") #should be ideally refered from reference table
		db.session.add(order_generated)
		db.session.commit()

		session['messages'] = temp
		#print(temp)
		#return render_template('added_to_cart.html')
		#redirect(url_for('members'))
		return redirect("/api/v1/checkout")
	return render_template('index.html')

@app.route('/api/v1/checkout')
def sendthree():
	order_id1 = str(session['messages'])
	cost = "300"
	return "Your order id is "+order_id1+" and total cost is"+cost

db.create_all()

#run the app
if __name__ == '__main__':
	app.run(debug=True,host='localhost', port=80)