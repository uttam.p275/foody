from flask import Flask, g, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from flask import Flask, render_template, request
import random

from flask import redirect
import requests
import time

app = Flask(__name__)


from py_zipkin.zipkin import zipkin_span, create_http_headers_for_new_span

'''
def http_transport(encoded_span):
    # The collector expects a thrift-encoded list of spans. Instead of
    # decoding and re-encoding the already thrift-encoded message, we can just
    # add header bytes that specify that what follows is a list of length 1.
    #body = "\x0c\x00\x00\x00\x01" + encoded_span
    requests.post(
        'http://localhost:9411/api/v1/spans',
        data="hi",
        headers={'Content-Type': 'application/x-thrift'},
    )
'''

#SQL Alchemy Engine
app.config['SQLALCHEMY_DATABASE_URI']='mysql+mysqlconnector://root:root@localhost:3306/uttam'
engine = create_engine('mysql+mysqlconnector://root:root@localhost:3306/uttam')

'''
@zipkin_span(service_name='webapp', span_name='do_stuff')
def do_stuff():
    time.sleep(5)
    headers = create_http_headers_for_new_span()
    requests.get('http://localhost:6000/service1/', headers=headers)
    return 'OK'
'''


# login
@app.route('/api/v1/login', methods=['GET', 'POST'])
def login():
	error = None
	if request.method == 'POST':
		if request.form['phone_cred'] == 'admin' and request.form['pass_cred'] == 'admin':
			'''
			with zipkin_span(service_name='webapp',span_name='index',transport_handler=http_transport,port=5000,sample_rate=100,):
				do_stuff()
				time.sleep(10)
			'''
			return redirect("http://localhost:80/api/v1/track", code=302)
            #return 'success'
	return render_template('login.html', error=error)


#run the app
if __name__ == '__main__':
    app.run(debug=True,host='localhost', port=81)