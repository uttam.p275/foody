#imports
from flask import Flask, g
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from flask import Flask, render_template, request
import random

app = Flask(__name__)

db = SQLAlchemy(app)

#SQL Alchemy Engine
app.config['SQLALCHEMY_DATABASE_URI']='mysql+mysqlconnector://root:root@localhost:3306/uttam'
engine = create_engine('mysql+mysqlconnector://root:root@localhost:3306/uttam')

#Database Model
class user_list(db.Model):
	phone = db.Column(db.Integer, primary_key=True)
	first_name = db.Column(db.String(200), nullable=False)
	last_name = db.Column(db.String(200), nullable=False)
	address = db.Column(db.String(200), nullable=False)
	password = db.Column(db.String(200), nullable=False)
	Email = db.Column(db.String(200), nullable=False)

#Routes
#route for user entry
@app.route('/api/v1/user_entry',methods=['GET','POST'])
def send():
	if request.method == 'POST':
		pn = request.form['pn']
		fn = request.form['fn']
		ln = request.form['ln']
		add = request.form['add']
		pwd = request.form['pwd']
		em = request.form['em']
		new_user = user_list(phone = pn,first_name = fn, last_name = ln, address = add, password = pwd, Email = em)
		db.session.add(new_user)
		db.session.commit()
		return "User Successfully registered!"
	return render_template('index_user_registration.html')



db.create_all()

#run the app
if __name__ == '__main__':
	app.run(debug=True)